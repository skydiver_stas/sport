<?php

use yii\db\Migration;

class m161206_115217_rename_column_category_club_id_to_kind_club_id extends Migration
{
    public function up()
    {
        $this->renameColumn('clubs', 'category_club_id', 'kind_club_id');
    }

    public function down()
    {
        $this->renameColumn('clubs', 'kind_club_id', 'category_club_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
