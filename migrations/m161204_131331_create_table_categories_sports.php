<?php

use yii\db\Migration;

class m161204_131331_create_table_categories_sports extends Migration
{
    public function up()
    {
        $this->createTable('categories_sports', [
            'id' => $this->primaryKey(),
            'name' => $this->string(60)->notNull(),
            'create_at' => $this->timestamp(),
            'update_at' => $this->timestamp(),
        ]);
    }

    public function down()
    {
        $this->dropTable('categories_sports');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
