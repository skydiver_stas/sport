<?php

use yii\db\Migration;

class m161130_113619_create_table_areas extends Migration
{
    public function up()
    {
        $this->createTable('areas', [
            'id' => $this->primaryKey(),
            'region_id' => $this->integer()->unsigned()->notNull(),
            'name' => $this->string(255)->notNull(),
            'create_at' => $this->timestamp(),
            'update_at' => $this->timestamp(),
        ]);
    }

    public function down()
    {
        $this->dropTable('areas');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
