<?php

use yii\db\Migration;

class m161201_111155_rename_column_street_city_district_city_region_in_table_clubs extends Migration
{
    public function up()
    {
        $this->renameColumn('clubs', 'street', 'street_id');
        $this->renameColumn('clubs', 'city', 'city_id');
        $this->renameColumn('clubs', 'district_city', 'district_city_id');
        $this->renameColumn('clubs', 'region', 'region_id');
    }

    public function down()
    {
        $this->renameColumn('clubs', 'street_id', 'street');
        $this->renameColumn('clubs', 'city_id', 'city');
        $this->renameColumn('clubs', 'district_city_id', 'district_city');
        $this->renameColumn('clubs', 'region_id', 'region');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
