<?php

use yii\db\Migration;

class m161130_114122_create_table_sreeets extends Migration
{
    public function up()
    {
        $this->createTable('streets', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->unsigned()->notNull(),
            'name' => $this->string(255)->notNull(),
            'create_at' => $this->timestamp(),
            'update_at' => $this->timestamp(),
        ]);
    }

    public function down()
    {
        $this->dropTable('streets');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
