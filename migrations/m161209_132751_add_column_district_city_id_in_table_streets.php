<?php

use yii\db\Migration;

class m161209_132751_add_column_district_city_id_in_table_streets extends Migration
{
    public function up()
    {
        $this->addColumn('streets', 'district_city_id', $this->integer()->unsigned()->after('city_id'));
    }

    public function down()
    {
        $this->dropColumn('streets', 'district_city_id');
    }


    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
