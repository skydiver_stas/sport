<?php

use yii\db\Migration;

class m161201_112834_create_table_districts_cities extends Migration
{
    public function up()
    {
        $this->createTable('districts_cities', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->unsigned()->notNull(),
            'name' => $this->string(255)->notNull(),
            'create_at' => $this->timestamp(),
            'update_at' => $this->timestamp(),
        ]);
    }

    public function down()
    {
        $this->dropTable('districts_cities');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
