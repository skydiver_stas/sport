<?php

use yii\db\Migration;

class m161130_113734_create_table_cities extends Migration
{
    public function up()
    {
        $this->createTable('cities', [
            'id' => $this->primaryKey(),
            'area_id' => $this->integer()->unsigned()->notNull(),
            'region_id' => $this->integer()->unsigned()->notNull(),
            'name' => $this->string(255)->notNull(),
            'create_at' => $this->timestamp(),
            'update_at' => $this->timestamp(),
        ]);
    }

    public function down()
    {
        $this->dropTable('cities');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
