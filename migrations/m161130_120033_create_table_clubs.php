<?php

use yii\db\Migration;

/**
 * Handles the creation of table `clubs`.
 */
class m161130_120033_create_table_clubs extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('clubs', [
            'id' => $this->primaryKey(),
            'club_owner_id' => $this->integer()->unsigned()->notNull(),
            'category_club_id' => $this->integer()->unsigned()->notNull(),
            'name' => $this->string(60),
            'description' => $this->text(),
            'contacts' => $this->text()->notNull(),
            'building_number' => $this->string(15)->notNull(),
            'street' => $this->integer()->unsigned()->notNull(),
            'city' => $this->integer()->unsigned()->notNull(),
            'district_city' => $this->integer()->unsigned()->notNull(),
            'region' => $this->integer()->unsigned()->notNull(),
            'coordinates' => $this->string(255)->notNull(),
            'photos' => $this->text(),
            'status' => $this->smallInteger()->notNull(),
            'create_at' => $this->timestamp(),
            'update_at' => $this->timestamp(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('clubs');
    }
}
