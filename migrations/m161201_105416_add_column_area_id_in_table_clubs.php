<?php

use yii\db\Migration;

class m161201_105416_add_column_area_id_in_table_clubs extends Migration
{
    public function up()
    {
        $this->addColumn('clubs', 'area_id', $this->integer()->unsigned()->notNull()->after('district_city'));
    }

    public function down()
    {
        $this->dropColumn('clubs', 'area_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
