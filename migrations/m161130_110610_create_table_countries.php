<?php

use yii\db\Migration;

class m161130_110610_create_table_countries extends Migration
{
    public function up()
    {
        $this->createTable('countries', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'short_name' => $this->string(5)->notNull(),
            'create_at' => $this->timestamp(),
            'update_at' => $this->timestamp(),
        ]);
    }

    public function down()
    {
        $this->dropTable('countries');

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
