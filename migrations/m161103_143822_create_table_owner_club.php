<?php

use yii\db\Migration;

class m161103_143822_create_table_owner_club extends Migration
{
    public function up()
    {
        $this->createTable('owner_club',[
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->unsigned()->notNull(),
            'name_club' => $this->string(),
            'create_at' => $this->timestamp(),
            'update_at' => $this->timestamp(),
        ]);
    }

    public function down()
    {
        $this->dropTable('owner_club');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
