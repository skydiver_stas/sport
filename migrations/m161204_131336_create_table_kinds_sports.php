<?php

use yii\db\Migration;

class m161204_131336_create_table_kinds_sports extends Migration
{
    public function up()
    {
        $this->createTable('kinds_sports', [
            'id' => $this->primaryKey(),
            'category_sport_id' => $this->integer()->unsigned()->notNull(),
            'name' => $this->string(60)->notNull(),
            'create_at' => $this->timestamp(),
            'update_at' => $this->timestamp(),
        ]);
    }

    public function down()
    {
        $this->dropTable('kinds_sports');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
