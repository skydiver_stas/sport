<?php

use yii\db\Migration;

class m161130_112208_create_table_regions extends Migration
{
    public function up()
    {
        $this->createTable('regions', [
            'id' => $this->primaryKey(),
            'country_id' => $this->smallInteger(1)->unsigned()->notNull(),
            'name' => $this->string(255)->notNull(),
            'create_at' => $this->timestamp(),
            'update_at' => $this->timestamp(),
        ]);
    }

    public function down()
    {
        $this->dropTable('regions');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
