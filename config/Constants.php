<?php
/**
 * Created by PhpStorm.
 * User: Sky
 * Date: 04.11.16
 * Time: 13:32
 */

namespace app\config;

class Constants {

    private $config;

    public function __construct()
    {
        $this->config = require ('web.php');
    }

    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE = 0;

    const USER_GROUP = 1;
    const OWNER_GROUP = 2;

    /* begin directories */


    const PATH_USER_AVATAR = 'images/avatar/';
    const PATH_USER_AVATAR_VIEW = '/sport/web/images/avatar/';

    const PATH_CLUBS_IMAGES = 'images/clubs/';
    const PATH_CLUBS_IMAGES_VIEW = 'images/clubs/';


    /* end directories */

    /* begin name file */

    const NOT_IMAGE_AVATAR = 'no_image.png';

    /* end name file */
}