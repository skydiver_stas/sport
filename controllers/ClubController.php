<?php

namespace app\controllers;

use app\models\CategorySport;
use app\models\KindSport;
use app\models\Club;

class ClubController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $categories = CategorySport::getCategories();
        return $this->render('index', [
            'categories' => $categories,
        ]);
    }

    public function actionKindSport($id) {
        $category_id = $id;
        $kinds_sport = KindSport::getKindsSport($category_id);
        $category_sport = CategorySport::getCategory($category_id);

        return $this->render('kinds_sports', [
            'kinds_sport' => $kinds_sport,
            'category_sport' => $category_sport,
        ]);
    }

    public function actionClubsList($id) {
        $kind_club_id = $id;
        $clubs_list = Club::getClubs($kind_club_id);
        $kind_sport = KindSport::getNameKind($kind_club_id);

        return $this->render('clubs_list', [
            'clubs_list' => $clubs_list,
            'kind_sport' => $kind_sport,
        ]);
    }

    public function actionClub($id)
    {
        $club = Club::getClub($id);
        //var_dump($club);
        return $this->render('club',[
            'club' => $club,
        ]);
    }

}
