<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\LoginForm;
use app\config\Constants;
use app\models\RegistrationToUser;
use app\models\RegistrationToClub;

class SecurityController extends \yii\web\Controller
{
    public $username;
    public $email;

    /**
     * Login action.
     *
     * @return string
     */

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionRegistration()
    {
        return $this->render('registration');
    }

    public function actionUser()
    {
        $model = new RegistrationToUser();

        if($model->load(Yii::$app->request->post()) && $model->validate())
        {
            if($user = $model->registration())
            {
                if (Yii::$app->getUser()->login($user))
                {
                    if($user->status === Constants::STATUS_ACTIVE)
                    {
                        return $this->goHome();
                    }
                }

            }
        }

        return $this->render('user', ['model' => $model]);
    }

    public function actionClub()
    {
        $model = new RegistrationToClub();

        if($model->load(Yii::$app->request->post()) && $model->validate())
        {

            if($user = $model->registration())
            {
                if (Yii::$app->getUser()->login($user))
                {
                    if($user->status === Constants::STATUS_ACTIVE)
                    {
                        return $this->goHome();
                    }
                }

            }
        }

        return $this->render('club', ['model' => $model]);
    }

}
