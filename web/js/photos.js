/**
 * Created by Sky on 19.12.16.
 */
$(document).ready(function(){

    var add_li = function(img) {
        $('#bock-img-modal-background #list-img').append(img);


    };

    var add_error = function(file) {
        console.log(info.maxFileSize);
        console.log(file.size);
        if(file.size > info.maxFileSize) {
            return 'перевыщено ліміт розміру';
        } else {
            return 'ok';
        }
    };

    var add_image = function(input) {
        if(input.files && input.files[0]) {
           var files_list = input.files;
            $('#list-img').empty();

            for(var i = 0; i < files_list.length; i++) {
                var file = files_list[i];
                var reader = new FileReader();
                var result;

                console.log(add_error(file));
                reader.onload = function(event) {
                    var error_image;
                    if(event.total > info.maxFileSize) {
                        error_image = 'перевыщено ліміт розміру';
                    } else {
                        error_image = 'ok';
                    }
                    result = '<div class="files-list"><img class="file-list" src="'+ event.target.result +'" /><div>'+ error_image +'</div></div>';
                    add_li(result);
                }
                reader.readAsDataURL(file);
            }
        }
    };

    $('#club-image').change(function(){
        add_image(this);
        $('#show-images').css('display', 'block');
        $('#bock-img-modal-background').css('display', 'block');
    });

    $('#close-list-img').click(function(){
        $('#bock-img-modal-background').css('display', 'none');
    });

    $('#show-images').click(function(){
        $('#bock-img-modal-background').css('display', 'block');
    });

});
