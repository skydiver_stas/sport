var initMap,
    map,
    addMarker,
    fillData,
    deleteData,
    marker = [],
    address_point = {},
    center_map = { lat: 48.5747899, lng: 31.9921875 },
    zoom = 6,
    infoWindow,
    messInfoWindow,
    isset_click = 0,
    test = 2;

$(document).ready(function(){
    $('.close-map').click(function(){
        $('.map-modal-background').hide();
    });
    $('.map-button').click(function(){
        $('.map-modal-background').show();
        initMap();
        if(isset_click === 1) {
            addMarker(center_map, map);
            infoWindow(messInfoWindow, map, marker[0]);
        }
    });

fillData = function (address_point) {

        var remClass = function(element){
            element.removeClass('hide-field');
        };

        if(address_point.building_number) {
            $('input#club-building_number').val(address_point.building_number);
            remClass($('.field-club-building_number'));
        }

        if(address_point.building_number === undefined) {
            $('input#club-building_number').val('');
            remClass($('.field-club-building_number'));
        }

        if(address_point.street_name) {

            if(address_point.street_name != 'Unnamed Road') {
                if(!$('div.field-club-street').hasClass('hide-field')) {
                    $('div.field-club-street').addClass('hide-field')
                }
                $('input#club-street').val(address_point.street_name);

            }
            if(address_point.street_name === 'Unnamed Road') {
                $('input#club-street').val('');
                remClass( $('div.field-club-street'));
            }
        }

        if(address_point.city) {
            $('input#club-city').val(address_point.city);
        } else {
            $('input#club-city').val('');
        }

        if(address_point.district_city) {
            $('input#club-district_city').val(address_point.district_city);
        } else {
            $('input#club-district_city').val('');
        }

        if(address_point.area) {
            $('input#club-area').val(address_point.area);
        }
        if(address_point.region) {
            $('input#club-region').val(address_point.region);
        }

        if(address_point.country) {
            $('input#club-country').val(address_point.country);
        }

        if(address_point.cntr) {
            $('input#club-country_short').val(address_point.cntr);
        }

        if(address_point.coordinates) {
            $('input#club-coordinates').val(address_point.coordinates);
        }


    }

});

deleteData = function() {
    $('input.hide-field').val('');
};

addMarker = function (location, map) {
    // Add the marker at the clicked location, and add the next-available label
    // from the array of alphabetical characters.

    marker[0] = new google.maps.Marker({
        position: location,
        map: map
    });
};

infoWindow = function (dataForInfWin, map, marker) {
    getInfoWindow = new google.maps.InfoWindow({
        content : dataForInfWin
    }).open(map, marker);
};

initMap = function () {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: zoom,
        center: center_map
    });

    // This event listener calls addMarker() when the map is clicked.
    google.maps.event.addListener(map, 'click', function(event) {

        var url = '';

        url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='+event.latLng.lat()+','+event.latLng.lng()+'&sensor=false';

        $.ajax({
            url: url,
            type: "GET",
            dataType: "json",
            success: function(data)
            {
                if(data)
                {
                    address_point = {};
                    if(result = data.results[0].address_components) {

                        for(var i = 0; i < result.length; i++) {
                            for(var j = 0; j < result[i].types.length; j++) {

                                switch(result[i].types[j]) {
                                    case 'street_number':
                                        address_point.building_number = result[i].long_name;
                                        break;

                                    case 'route':

                                        var street_arr = result[i].long_name.split(' '),
                                            street_str = '';

                                        for(var w = 0; w < street_arr.length; w++) {
                                            if(street_arr[w] === 'вулиця') {
                                                delete street_arr[w];
                                                continue ;
                                            }
                                            street_str += street_arr[w] + ' ';
                                        }
                                        street_str = street_str.substring(0, street_str.length - 1);
                                        address_point.street_name = street_str;
                                        break;

                                    case 'sublocality_level_1':

                                        var district_arr = result[i].long_name.split(' '),
                                            district_str = '';

                                        for(var q = 0; q < district_arr.length; q++) {
                                            if(district_arr[q] === 'район') {
                                                delete district_arr[q];
                                                continue ;
                                            }
                                            district_str += district_arr[q];
                                        }

                                        address_point.district_city = district_str;
                                        break;

                                    case 'administrative_area_level_2':
                                        var str = result[i].long_name.split(' ');
                                        if(str[0] === 'місто') {
                                            address_point.area = str.pop()+'ський';
                                        }
                                        else if(str.pop() === 'район') {
                                            address_point.area = str.shift();
                                        }
                                        break;

                                    case 'administrative_area_level_1':
                                        var str = result[i].long_name.split(' ');
                                        if(str[0] === 'місто') {
                                            address_point.region = str.pop()+'ська';
                                        }
                                        else if(str.pop() === 'область') {
                                            address_point.region = str.shift();
                                        }
                                        break;

                                    case 'locality':
                                        address_point.city = result[i].long_name;
                                        break;

                                    case 'country':
                                        address_point.country = result[i].long_name;
                                        address_point.cntr = result[i].short_name;
                                        break;
                                }

                            }

                        }
                        address_point.coordinates = event.latLng.lat() +' '+ event.latLng.lng();
                    }
                    if(!address_point.area && address_point.city) {
                        address_point.area = address_point.city+'вський';
                    }

                    if(!address_point.area || !address_point.region || !address_point.city) {
                        address_point = undefined;

                    }

                    if(address_point) {
                        if(address_point.cntr != 'UA') {
                            address_point = undefined;
                        }

                        if(marker[0] && address_point.cntr === 'UA')
                            marker[0].setMap(null);
                        center_map = event.latLng;
                        zoom = map.getZoom();
                        isset_click = 1
                        messInfoWindow =  '<span class="div-read">номер будинку: </span>'+ address_point.building_number +
                                          '<br /><span class="div-read"> вулиця: </span>'+address_point.street_name +
                                          '<br /><span class="div-read"> місто: </span>' + address_point.city;

                        addMarker(event.latLng, map);
                        infoWindow(messInfoWindow, map, marker[0]);
                        fillData(address_point);
                    } else {
                        deleteData();
                    }


console.log(address_point);
                }

            }
        });
    });

};