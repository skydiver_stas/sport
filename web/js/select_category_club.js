$(document).ready(function(){
    $('#club-category_sport').change(function(){
    var url = $('#url').data('url');
        console.log(url);
        $.ajax({
            url: url + this.value,
            type: 'GET',
            dataType: 'json',

            success: function(data){
                $('.field-club-kind_sport').removeClass('hide-field');
                $('#club-kind_sport option').remove();
                
                var arr = Object.values(data)
                for(var i = 0; i < arr.length; i++) {
                    $('#club-kind_sport').append($('<option>', {
                        value:arr[i].id,
                        text:arr[i].name
                    }));
                }
            }
        });

    });
});