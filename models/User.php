<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use app\models\OwnerClub;

class User extends ActiveRecord implements IdentityInterface
{

    public $auth_key;
    public $access_token;

    public static function tableName()
    {
        return 'users';
    }


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return self::findOne([
            'id' => $id
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return self::findOne([
            'access_token' => $token,
        ]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::findOne([
            'username' => $username,
        ]);
    }

    public static function findByEmail($email)
    {
        return self::findOne([
            'email' => $email,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function generateAutKey()
    {
        $this->aut_key = Yii::$app->security->generateRandomString();
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($auth_key)
    {
        return $this->auth_key === $auth_key;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    public function updatePassword($id, $password)
    {
        $user = self::findOne(['id' => $id,]);
        $user->password = $password;
        return $user->update();
    }

    public function getOwner()
    {
        return $this->hasOne(OwnerClub::className(), ['user_id' => 'id']);
    }
}
