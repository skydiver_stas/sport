<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clubs".
 *
 * @property integer $id
 * @property integer $club_owner_id
 * @property integer $kind_club_id
 * @property string $name
 * @property string $description
 * @property string $contacts
 * @property string $building_number
 * @property integer $street_id
 * @property integer $city_id
 * @property integer $district_city_id
 * @property integer $area_id
 * @property integer $region_id
 * @property string $coordinates
 * @property string $photos
 * @property integer $status
 * @property string $create_at
 * @property string $update_at
 */
class Club extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clubs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['club_owner_id', 'kind_club_id', 'contacts', 'building_number', 'street_id', 'city_id', 'district_city_id', 'area_id', 'region_id', 'coordinates', 'status'], 'required'],
            [['club_owner_id', 'kind_club_id', 'street_id', 'city_id', 'district_city_id', 'area_id', 'region_id', 'status'], 'integer'],
            [['description', 'contacts', 'photos'], 'string'],
            [['create_at', 'update_at'], 'safe'],
            [['name'], 'string', 'max' => 60],
            [['building_number'], 'string', 'max' => 15],
            [['coordinates'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'club_owner_id' => 'Club Owner ID',
            'kind_club_id' => 'Kind Club ID',
            'name' => 'Name',
            'description' => 'Description',
            'contacts' => 'Contacts',
            'building_number' => 'Building Number',
            'street_id' => 'Street ID',
            'city_id' => 'City ID',
            'district_city_id' => 'District City ID',
            'area_id' => 'Area ID',
            'region_id' => 'Region ID',
            'coordinates' => 'Coordinates',
            'photos' => 'Photos',
            'status' => 'Status',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
        ];
    }

    public static function getClubs($kind_club_id)
    {
        return self::find()
            ->asArray()
            ->where(['kind_club_id' => $kind_club_id])
            ->all();
    }

    public static function getClub($id)
    {
        return self::find()
            ->where(['id' => $id])
            ->one();
    }
}
