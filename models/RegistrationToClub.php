<?php
/**
 * Created by PhpStorm.
 * User: Sky
 * Date: 01.11.16
 * Time: 16:34
 */

namespace app\models;

use Yii;
use yii\base\Model;
use app\config\Constants;
use app\models\User;
use app\models\OwnerClub;

/**
 * Class RegistrationFormClub
 * @package app\models
 */
class RegistrationToClub extends Model {

    public $username;
    public $email;
    public $password;
    public $repeatPassword;
    public $status;
    public $clubName;

    public function rules()
    {
        return [
            [['clubName', 'username', 'email', 'password', 'repeatPassword'], 'filter', 'filter' => 'trim'],
            [['clubName','username', 'email', 'password', 'repeatPassword'], 'required'],
            ['username', 'string', 'min' => 4, 'max' => 120],
            ['email', 'email'],
            [['password', 'repeatPassword'], 'required', 'on' => 'create'],
            [['password', 'repeatPassword'], 'validatePassword'],
            ['username', 'unique',
                'targetClass' => User::className(),
                'message' => 'Это имя уже занято.'],
            ['email', 'email'],
            ['email', 'unique',
                'targetClass' => User::className(),
                'message' => 'Эта почта уже занята.'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'clubName' => 'Назва клубу який ви представляєте',
            'username' => 'Імя користувача',
            'email' => 'Е-почта',
            'password' => 'Пароль',
            'repeatPassword' => 'Повторити пароль'
        ];
    }

    /**
     *   for validation password in case coincidence
     *   для валідації паролю на співпадання
    */
    public function validatePassword($attribute, $params)
    {
        if($this->password != $this->repeatPassword)
        {
            $this->addError($attribute, 'Паролі не співпадають');
        }
    }

    /**
     *  registration user as owner club
     *  реестрація користувача який є "власником" клубу
    */
    public function registration()
    {

        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAutKey();
        $user->status = Constants::STATUS_ACTIVE;
        $user->user_group = Constants::OWNER_GROUP;

        if($user->save())
        {
            $userFromDb = User::findByUsername($this->username);
            $owner = new OwnerClub();
            $owner->user_id = $userFromDb->id;
            $owner->name_club = $this->clubName;
            $owner->save();

            $userData = new UserData();
            $userData->user_id = $userFromDb->id;
            $userData->save();


            if($user->save() && $owner->save() && $userData->save())
            {
                return $user->save() ? $user : null;
            }  else
            {
                $userDelete = User::findOne(['username' => $this->username]);
                $userDelete->delete();
                return false;
            }

        }

        //return $user->save() ? $user : null;

    }

} 