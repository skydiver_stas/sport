<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\config\Constants;
use app\models\User;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class RegistrationToUser extends Model
{
    public $username;
    public $email;
    public $password;
    public $repeatPassword;
    public $status;

    public function rules()
    {
        return [
            [['username', 'email', 'password', 'repeatPassword'], 'filter', 'filter' => 'trim'],
            [['username', 'email', 'password', 'repeatPassword'], 'required'],
            ['username', 'string', 'min' => 4, 'max' => 120],
            ['email', 'email'],
            [['password', 'repeatPassword'], 'required', 'on' => 'create'],
            [['password', 'repeatPassword'], 'validatePassword'],
            ['username', 'unique',
                'targetClass' => User::className(),
                'message' => 'Это имя уже занято.'],
            ['email', 'email'],
            ['email', 'unique',
                'targetClass' => User::className(),
                'message' => 'Эта почта уже занята.'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Імя користувача',
            'email' => 'Е-почта',
            'password' => 'Пароль',
            'repeatPassword' => 'Повторити пароль'
        ];
    }


    public function validatePassword($attribute, $params)
    {
        if($this->password != $this->repeatPassword)
        {
            $this->addError($attribute, 'Паролі не співпадають');
        }
    }


    public function registration()
    {
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAutKey();
        $user->status = Constants::STATUS_ACTIVE;
        $user->user_group = Constants::USER_GROUP;

        if($user->save())
        {
            $userFromDb = User::findByUsername($this->username);
            $userData = new UserData();
            $userData->user_id = $userFromDb->id;
            $userData->save();


            if($user->save() && $userData->save())
            {
                return $user->save() ? $user : null;
            }  else
            {
                $userDelete = User::findOne(['username' => $this->username]);
                $userDelete->delete();
                return false;
            }

        }
    }

    public function findByUsername()
    {
        $user = new User();
        return $user->findByUsername($this->username);
    }
}
