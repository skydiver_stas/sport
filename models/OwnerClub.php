<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "owner_club".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name_club
 * @property string $create_at
 * @property string $update_at
 */
class OwnerClub extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'owner_club';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['create_at', 'update_at'], 'safe'],
            [['name_club'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name_club' => 'Name Club',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
        ];
    }
}
