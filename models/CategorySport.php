<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categories_sports".
 *
 * @property integer $id
 * @property string $name
 * @property string $create_at
 * @property string $update_at
 */
class CategorySport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories_sports';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['create_at', 'update_at'], 'safe'],
            [['name'], 'string', 'max' => 60],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
        ];
    }

    public static function getCategories()
    {
        return self::find()
            ->asArray()
            ->orderBy("CONVERT({{name}} USING cp1251)")
            ->all();
    }

    public static function getCategory($id)
    {
        return self::findOne(['id' => $id]);
    }
}
