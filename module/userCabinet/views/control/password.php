<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use mylibrarys\ModalWindow;
$this->title = 'Зміна паролю';
$this->params['breadcrumbs'][] = ['label' => 'Кабінет', 'url' => ['/user-cabinet']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="change_password">

    <?php
        if($modal === 1)
            echo ModalWindow::WindowMessage($this, 'Пароль успішно змінений');

     ?>

    <?php $form = ActiveForm::begin([

    ]); ?>

    <?= $form->field($model, 'old_password')->passwordInput() ?>

    <?= $form->field($model, 'new_password_1')->passwordInput() ?>

    <?= $form->field($model, 'new_password_2')->passwordInput() ?>



    <div class="form-group">

            <?= Html::submitButton('Змінити', ['class' => '', 'name' => 'btn']) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>