<?php


use yii\helpers\Html;
use app\config\Constants;
use mylibrarys\CheckImage;

$this->title = 'Кабінет';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="userCabinet-default-index">

    <div class="profile">
        <div class="image_profile">
            <img src="<?= CheckImage::checkFileAvatar(Constants::PATH_USER_AVATAR_VIEW, $user_data->userData->photo, Constants::NOT_IMAGE_AVATAR) ?>" >
        </div>
        <div class="user_name">
            Користувач: <br>
            <span><?= $user_data->username; ?></span>
        </div>

        <div class="user_data">
            <hr>
            <strong>Про Користувача:</strong>
                <p>
                    Зареєстрований: <?= substr($user_data->userData->create_at, 0, 10); ?>
                </p>

            <hr>
            <strong>Керування профілем:</strong>
                <p>
                    <?= Html::a('Змінити аватар', ['/user-cabinet/control/photo']) ?> <br>
                    <?= Html::a('Змінити пароль', ['/user-cabinet/control/password']) ?>


                </p>
        </div>


    </div>


</div>
