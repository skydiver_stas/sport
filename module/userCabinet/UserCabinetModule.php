<?php

namespace app\module\userCabinet;

/**
 * userCabinet module definition class
 */
class UserCabinetModule extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\module\userCabinet\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
