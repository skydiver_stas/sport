<?php
/**
 * Created by PhpStorm.
 * User: Sky
 * Date: 04.11.16
 * Time: 11:31
 */

namespace app\module\userCabinet;

use Yii;
use yii\web\Controller;
use app\config\Constants;

class Access extends Controller{

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action))
        {
            return false;
        }

        if (!Yii::$app->user->isGuest)
        {
            if(Yii::$app->user->identity['user_group'] == Constants::USER_GROUP && Yii::$app->user->identity['status'] == Constants::STATUS_ACTIVE)
            {
                return true;
            }

            if(Yii::$app->user->identity['user_group'] == Constants::OWNER_GROUP && Yii::$app->user->identity['status'] == Constants::STATUS_ACTIVE)
            {
                Yii::$app->getResponse()->redirect('owner-cabinet');
                return false;
            }

            else
            {
                Yii::$app->getResponse()->redirect('site/not-active');
                return false;
            }

        }
        else
        {
            Yii::$app->getResponse()->redirect('security/login');
            //для перестраховки вернем false
            return false;
        }
    }

} 