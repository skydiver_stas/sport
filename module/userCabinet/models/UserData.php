<?php

namespace app\module\userCabinet\models;

use Yii;
use app\module\userCabinet\models\UserCabinet;

/**
 * This is the model class for table "user_data".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $photo
 * @property string $create_at
 * @property string $update_at
 */
class UserData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['create_at', 'update_at'], 'safe'],
            [['photo'], 'file', 'maxSize' => 32000 ,'extensions' => 'png, gif, jpg, jpeg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'photo' => 'Photo',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(UserCabinet::className(), ['id' => 'user_id']);
    }

    public static function getPhoto($user_id)
    {
        $result = self::find()
            ->asArray()
            ->select(['photo'])
            ->where(['user_id' => $user_id])
            ->one();

        $result = array_shift($result);
        return $result;
    }

    public function updatePhoto($user_id, $name_photo)
    {
         $user = self::findOne(['user_id' => $user_id]);
         $user->photo = $name_photo;
         return $user->update();
    }
}
