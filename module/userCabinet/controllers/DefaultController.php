<?php

namespace app\module\userCabinet\controllers;

use Yii;
use app\module\userCabinet\Access;
use app\module\userCabinet\models\UserCabinet;

/**
 * Default controller for the `userCabinet` module
 */
class DefaultController extends Access
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $user_data = UserCabinet::getUser(Yii::$app->user->identity->id);

        return $this->render('index', [
            'user_data' => $user_data,
        ]);
    }


}
