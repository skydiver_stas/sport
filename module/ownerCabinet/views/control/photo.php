<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\config\Constants;
use mylibrarys\CheckImage;
use mylibrarys\ModalWindow;
use app\module\ownerCabinet\models\UserData;
/* @var $this yii\web\View */
$this->title = 'Зміна аватара';
$this->params['breadcrumbs'][] = ['label' => 'Кабінет', 'url' => ['/user-cabinet']];
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('@web/js/photo.js', ['depends' => 'yii\web\JqueryAsset']);
?>
<div class="change_photo">

    <?php
    if($modal === 1)
        echo ModalWindow::WindowMessage($this, 'Ваша аватарка успішно змінена');

    ?>

    <strong>Виберіть зображення:</strong>
    <hr>
    <p>
        <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
        <?= $form->field($model, 'photo')->fileInput(['id' => 'imgInp']) ?>
        <span>Рекомендований розмір зображення 256 * 256 і 32кб</span>
    <div>
        <div class="image_profile">
            <img id="blah" src="<?= CheckImage::checkFileAvatar(Constants::PATH_USER_AVATAR_VIEW, UserData::getPhoto(Yii::$app->getUser()->id), Constants::NOT_IMAGE_AVATAR) ?>" >
        </div>
    </div>
    <br>
    <?= Html::submitButton('Змінити', ['class' => 'btn-send', 'name' => 'photo']) ?>

    <?php ActiveForm::end(); ?>
    </p>

</div>
