<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mylibrarys\ModalMap;
use mylibrarys\ModalListImage;
use app\module\ownerCabinet\models\CategorySport;
/* @var $this yii\web\View */
/* @var $model app\module\ownerCabinet\models\Club */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile('@web/js/select_category_club.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('@web/js/photos.js', ['depends' => 'yii\web\JqueryAsset']);
?>

<script type="text/javascript">var info = {maxFileSize: <?= json_decode($model->maxFileSize) ?>}</script>
<span id="url" class="hide-field" data-url="<?= \yii\helpers\Url::toRoute(['/owner-cabinet/control-club/get-kind-sport/', 'id' => '']) ?>"></span>
<div class="club-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'category_sport')->dropDownList(CategorySport::getAllCategories() ,['prompt' => 'Виберіть категорію...'])->label() ?>

    <?= $form->field($model, 'kind_sport', ['options' => ['class' => 'hide-field']])->dropDownList(['prompt' => 'Виберіть категорію...']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'contacts')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'image[]')->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>

    <?= ModalListImage::ShowImages() ?>

    <?= Html::button('Показати вибрані фото', ['id' => 'show-images']) ?>

    <hr />

    <?= Html::button('Вибрати розташування на карті', ['class' => 'map-button']) ?>

    <?= $form->field($model, 'building_number', ['options' => ['class' => 'hide-field']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'street', ['options' => ['class' => 'hide-field']])->textInput(['class' => 'form-control ']) ?>

    <?= $form->field($model, 'city', ['options' => ['class' => 'hide-field']])->textInput(['class' => 'hide-field form-control']) ?>

    <?= $form->field($model, 'district_city', ['options' => ['class' => 'hide-field']])->textInput(['class' => 'hide-field form-control']) ?>

    <?= $form->field($model, 'area', ['options' => ['class' => 'hide-field']])->textInput(['class' => 'hide-field form-control']) ?>

    <?= $form->field($model, 'region', ['options' => ['class' => 'hide-field']])->textInput(['class' => 'hide-field form-control']) ?>

    <?= $form->field($model, 'country_short', ['options' => ['class' => 'hide-field']])->textInput(['class' => 'hide-field form-control']) ?>

    <?= $form->field($model, 'country', ['options' => ['class' => 'hide-field']])->textInput(['class' => 'hide-field form-control']) ?>

    <?= $form->field($model, 'coordinates', ['options' => ['class' => 'hide-field']])->textInput(['class' => 'hide-field form-control']) ?>

    <div class="form-group">
        <hr/>
        <?= Html::submitButton($nameButton, ['class' => 'btn-send', 'name' => 'photo']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
