<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\module\ownerCabinet\models\SearchClub */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Управління клубами';
$this->params['breadcrumbs'][] = ['label' => 'Кабінет', 'url' => ['/owner-cabinet']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="club-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Додати клуб', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'club_owner_id',
            'kind_club_id',
            'name',
            'description:ntext',
            // 'contacts:ntext',
            // 'building_number',
            // 'street',
            // 'city',
            // 'district_city',
            // 'region',
            // 'coordinates',
            // 'photos:ntext',
            // 'status',
            // 'create_at',
            // 'update_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
