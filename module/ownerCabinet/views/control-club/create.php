<?php

use yii\helpers\Html;
use mylibrarys\ModalMap;

/* @var $this yii\web\View */
/* @var $model app\module\ownerCabinet\models\Club */

$this->title = 'Додати клуб';
$this->params['breadcrumbs'][] = ['label' => 'Кабінет', 'url' => ['/owner-cabinet']];
$this->params['breadcrumbs'][] = ['label' => 'Управління клубами', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= ModalMap::ShowMap($this) ?>

<div class="club-create">

    <h3><?= Html::encode($this->title) ?>:</h3>
    <hr>

    <?= $this->render('_form', [
        'model' => $model,
        'nameButton' => $nameButton,
    ]) ?>

</div>
