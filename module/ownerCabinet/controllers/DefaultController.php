<?php

namespace app\module\ownerCabinet\controllers;

use Yii;
use app\module\ownerCabinet\Access;
use app\module\ownerCabinet\models\UserCabinet;


/**
 * Default controller for the `ownerCabinet` module
 */
class DefaultController extends Access
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $user_data = UserCabinet::getUser(Yii::$app->user->identity->id);

        return $this->render('index', [
            'user_data' => $user_data,
        ]);
    }
}
