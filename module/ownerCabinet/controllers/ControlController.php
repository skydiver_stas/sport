<?php

namespace app\module\ownerCabinet\controllers;

use Yii;
use app\config\Constants;
use app\module\ownerCabinet\models\UserData;
use app\module\ownerCabinet\models\UserCabinet;
use yii\web\UploadedFile;
use app\models\UploadForm;
use app\models\User;

class ControlController extends \app\module\ownerCabinet\Access
{
    public function actionPhoto()
    {
        $model = new UserData();
        $modal = 0;
        $oldNamePhoto = UserData::getPhoto(Yii::$app->getUser()->id);

        if (Yii::$app->request->post())
        {

            if(UploadedFile::getInstance($model, 'photo'))
            {
                $namePhoto = uniqid();
                $photo = UploadedFile::getInstance($model, 'photo');
                $photoName = $namePhoto.'.'.$photo->extension;

                if($photo->saveAs(Constants::PATH_USER_AVATAR.$namePhoto.'.'.$photo->extension) && $model->updatePhoto(Yii::$app->getUser()->id ,$photoName))
                {
                    if(file_exists(Constants::PATH_USER_AVATAR.$oldNamePhoto))
                    {
                        unlink(Constants::PATH_USER_AVATAR.$oldNamePhoto);
                        $modal = 1;
                    }
                }
            }
        }

        return $this->render('photo', [
            'model' => $model,
            'modal' => $modal,
        ]);
    }

    public function actionPassword()
    {
        $modal = 0;
        $model = new UserCabinet();
        $modelUser = new User();

        if($model->load(Yii::$app->request->post()) && $model->validate())
        {

            if($modelUser->updatePassword(Yii::$app->user->identity->id ,Yii::$app->getSecurity()->generatePasswordHash(Yii::$app->request->post()['UserCabinet']['new_password_1'])))
            {
                $user = Yii::$app->user->identity->username;
                Yii::$app->user->logout();
                Yii::$app->getUser()->login($modelUser->findByUsername($user));
                $modal = 1;

            }

        }

        return $this->render('password',[
            'model' => $model,
            'modal' => $modal,

        ]);
    }

}
