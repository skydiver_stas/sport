<?php

namespace app\module\ownerCabinet\controllers;


use Yii;
use app\config\Constants;
use app\module\ownerCabinet\models\Club;
use app\module\ownerCabinet\models\SearchClub;
use app\module\ownerCabinet\Access;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\module\ownerCabinet\models\KindSport;
use yii\db\Query;
use mylibrarys\DeleteSymbol;
use yii\web\UploadedFile;

/**
 * ControlClubController implements the CRUD actions for Club model.
 */
class ControlClubController extends Access
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Club models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchClub();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Club model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Club model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Club();
        $nameButton = 'Створити';

        if ($model->load(Yii::$app->request->post())) {
            $request = Yii::$app->request->post('Club');

            foreach($request as $key => $value) {
                if($key != 'district_city' && empty($value) && $key != 'image')
                    return false;
            }

            $status = Constants::STATUS_ACTIVE;
            $owner_id = Yii::$app->user->identity->owner['id'];
            $kind_sport = trim(htmlspecialchars(stripcslashes($request['kind_sport'])));
            $name = trim(htmlspecialchars(stripcslashes($request['name'])));
            $description = trim(htmlspecialchars(stripcslashes($request['description'])));
            $contacts = trim(htmlspecialchars(stripcslashes($request['contacts'])));
            $number_building = trim(htmlspecialchars(stripcslashes($request['building_number'])));
            $coordinates = trim(htmlspecialchars(stripcslashes($request['coordinates'])));

            $request_country = DeleteSymbol::DeleteApostrophe($request['country']);
            $request_country_short = DeleteSymbol::DeleteApostrophe($request['country_short']);
            $request_region = DeleteSymbol::DeleteApostrophe($request['region']);
            $request_area = DeleteSymbol::DeleteApostrophe($request['area']);
            $request_city = DeleteSymbol::DeleteApostrophe($request['city']);
            $request_district_city = DeleteSymbol::DeleteApostrophe($request['district_city']);
            $request_street = DeleteSymbol::DeleteApostrophe($request['street']);


            $query = (new Query());

            $country_id = null;
            $country_result = null;

            $region_result = null;
            $region_id = null;

            $area_result = null;
            $area_id = null;

            $city_result = null;
            $city_id = null;

            $disrtict_city_result = null;
            $disrtict_city_id = null;
;
            $street_result = null;
            $street_id = null;

            $country_result = $query
                ->select('id')
                ->from('countries')
                ->where(['short_name' => $request_country_short, 'name' => $request_country])
                ->one();

            if($country_result) {
                $country_id = $country_result['id'];
            }


            $region_result = $query
                ->select(['id', 'country_id'])
                ->from('regions')
                ->where(['name' => $request_region])
                ->one();
            if($region_result) {
                $region_id = $region_result['id'];
                if($country_id == null || $country_id != $region_result['country_id']) {
                    $region_result = null;
                    $region_id = null;
                }
            }

            $area_result = $query
                ->select(['id', 'region_id'])
                ->from('areas')
                ->where(['name' => $request_area])
                ->one();
            if($area_result) {
                $area_id = $area_result['id'];
                if($region_id == null || $region_id != $area_result['region_id']) {
                    $area_result = null;
                    $area_id == null;
                }
            }

            $city_result = $query
                ->select(['id', 'area_id', 'region_id'])
                ->from('cities')
                ->where(['name' => $request_city])
                ->one();
            if($city_result) {
                $city_id = $city_result['id'];
                if($area_id == null || $area_id != $city_result['area_id'] && $region_id != $city_result['region_id']) {
                    $city_result = null;
                    $city_id = null;
                }
            }

            if(!empty($request_district_city)) {
                $disrtict_city_result = $query
                    ->select(['id', 'city_id'])
                    ->from('districts_cities')
                    ->where(['name' => $request_district_city])
                    ->one();
                if($disrtict_city_result) {
                    $disrtict_city_id = $disrtict_city_result['id'];
                    if($city_id == null || $city_id != $disrtict_city_result['city_id']) {
                        $disrtict_city_resultt = null;
                        $disrtict_city_id = null;
                    }
                }
            } else {
                $disrtict_city_id = 0;
            }

            $street_result = $query
                ->select(['id', 'city_id'])
                ->from('streets')
                ->where(['name' => $request_street])
                ->one();
            if($street_result) {
                $street_id = $street_result['id'];
                if($city_id == null || $city_id != $street_result['city_id']) {
                    $street_result = null;
                    $street_id = null;
                }
            }

            $images_name = [];

            if(UploadedFile::getInstance($model, 'image[0]')) {

                $i = 0;
                while(UploadedFile::getInstance($model, 'image['. $i .']')) {
                    $image_name = uniqid();
                    $image = UploadedFile::getInstance($model, 'image['. $i .']');
                    $image->saveAs(Constants::PATH_CLUBS_IMAGES.$image_name.'.'.$image->extension);
                    $images_name[$i] = $image_name.'.'.$image->extension;
                    $i++;

                }

            }

            $images_name = serialize($images_name);

            /* the begin transaction  */

            $db = Yii::$app->db;
            $transaction = $db->beginTransaction();

            try {

                //the beginning add address in db

                if(!$country_result) {
                    $db->createCommand("INSERT INTO countries (name, short_name) VALUES ('$request_country', '$request_country_short')")->execute();
                    $country_id = $db->getLastInsertID();
                }

                if(!$region_result) {
                    $db->createCommand("INSERT INTO regions (country_id, name) VALUES ('$country_id', '$request_region')")->execute();
                    $region_id = $db->getLastInsertID();
                }

                if(!$area_result) {
                    $db->createCommand("INSERT INTO areas (region_id, name) VALUES ('$region_id', '$request_area')")->execute();
                    $area_id = $db->getLastInsertID();
                }

                if(!$city_result) {
                    $db->createCommand("INSERT INTO cities (area_id, region_id, name) VALUES ('$area_id', '$region_id', '$request_city')")->execute();
                    $city_id = $db->getLastInsertID();
                }

                if(!$disrtict_city_result && !empty($request_district_city)) {
                    $db->createCommand("INSERT INTO districts_cities (city_id, name) VALUES ('$city_id', '$request_district_city')")->execute();
                    $disrtict_city_id = $db->getLastInsertID();
                }

                if(!$street_result) {
                    $db->createCommand("INSERT INTO streets (district_city_id, city_id, name) VALUES ('$disrtict_city_id', '$city_id', '$request_street')")->execute();
                    $street_id = $db->getLastInsertID();
                }

                //end add address in db

                // the beginning add club information
                $db->createCommand("INSERT INTO clubs (club_owner_id, kind_club_id, name, description, contacts, building_number, street_id, city_id, district_city_id, area_id, region_id, coordinates, photos, status)
                                    VALUES ('$owner_id', '$kind_sport', '$name', '$description', '$contacts', '$number_building', '$street_id', '$city_id', '$disrtict_city_id', '$area_id', '$region_id', '$coordinates', '$images_name', '$status')
                ")->execute();

                // end add club information


                $transaction->commit();

            } catch(\Exception $e) {
                $transaction->rollBack();

                $images_name = unserialize($images_name);
                if(!empty($images_name)) {
                    for($i = 0; $i < count($images_name); $i++) {
                        unlink(Constants::PATH_CLUBS_IMAGES.$images_name[$i]);
                    }
                }

                throw $e;
            }

            /* end begin transaction  */

            return $this->render('create', [
                'model' => $model,
                'nameButton' => $nameButton,
            ]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'nameButton' => $nameButton,
            ]);
        }
    }

    /**
     * Updates an existing Club model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Club model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Club model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Club the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Club::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetKindSport($id)
    {
        echo json_encode(KindSport::getKindSport($id));
    }
}
