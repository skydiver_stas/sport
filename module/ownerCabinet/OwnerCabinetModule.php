<?php

namespace app\module\ownerCabinet;

/**
 * ownerCabinet module definition class
 */
class OwnerCabinetModule extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\module\ownerCabinet\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
