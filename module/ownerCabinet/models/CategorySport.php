<?php

namespace app\module\ownerCabinet\models;

use Yii;

/**
 * This is the model class for table "categories_sports".
 *
 * @property integer $id
 * @property string $name
 * @property string $create_at
 * @property string $update_at
 */
class CategorySport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories_sports';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['create_at', 'update_at'], 'safe'],
            [['name'], 'string', 'max' => 60],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
        ];
    }

    public static function getAllCategories() {
        $list = self::find()
            ->asArray()
            ->select(['id' ,'name'])
            ->all();
        $category_list = [];

        foreach($list as $key) {
            $name = ucfirst($key['name']);
            $category_list += [$key['id'] => $name];
        }
        return $category_list;
    }
}
