<?php

namespace app\module\ownerCabinet\models;

use Yii;

/**
 * This is the model class for table "districts_cities".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $name
 * @property string $create_at
 * @property string $update_at
 */
class District extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'districts_cities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'name'], 'required'],
            [['city_id'], 'integer'],
            [['create_at', 'update_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'name' => 'Name',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
        ];
    }
}
