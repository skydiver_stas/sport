<?php

namespace app\module\ownerCabinet\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\module\ownerCabinet\models\Club;

/**
 * SearchClub represents the model behind the search form about `app\module\ownerCabinet\models\Club`.
 */
class SearchClub extends Club
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'club_owner_id', 'kind_club_id', 'street', 'city', 'district_city', 'region', 'status'], 'integer'],
            [['name', 'description', 'contacts', 'building_number', 'coordinates', 'photos', 'create_at', 'update_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Club::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'club_owner_id' => $this->club_owner_id,
            'kind_club_id' => $this->kind_club_id,
            'street' => $this->street,
            'city' => $this->city,
            'district_city' => $this->district_city,
            'region' => $this->region,
            'status' => $this->status,
            'create_at' => $this->create_at,
            'update_at' => $this->update_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'contacts', $this->contacts])
            ->andFilterWhere(['like', 'building_number', $this->building_number])
            ->andFilterWhere(['like', 'coordinates', $this->coordinates])
            ->andFilterWhere(['like', 'photos', $this->photos]);

        return $dataProvider;
    }
}
