<?php

namespace app\module\ownerCabinet\models;

use Yii;

/**
 * This is the model class for table "clubs".
 *
 * @property integer $id
 * @property integer $club_owner_id
 * @property integer $category_club_id
 * @property string $name
 * @property string $description
 * @property string $contacts
 * @property string $building_number
 * @property integer $street
 * @property integer $city
 * @property integer $district_city
 * @property integer $region
 * @property string $coordinates
 * @property string $photos
 * @property integer $status
 * @property string $create_at
 * @property string $update_at
 */
class Club extends \yii\db\ActiveRecord
{

    public $street;
    public $city;
    public $district_city;
    public $area;
    public $region;
    public $country;
    public $country_short;
    public $category_sport;
    public $kind_sport;
    public $image;
    public $maxFileSize = 135000;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clubs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'category_sport', 'contacts', 'building_number', 'street', 'city', 'area', 'region', 'country', 'country_short', 'coordinates'], 'required'],
            [['name', 'description', 'contacts', 'photos', 'street', 'city', 'district_city', 'area', 'region', 'countries', 'category_sport'], 'string'],
            [['create_at', 'update_at'], 'safe'],
            [['name'], 'string', 'max' => 60],
            [['building_number'], 'string', 'max' => 15],
            [['coordinates'], 'string', 'max' => 255],
            [['image'],  'file', 'extensions' => 'gif, jpg, jpeg, png', 'maxSize' => $this->maxFileSize, 'maxFiles' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'club_owner_id' => 'Club Owner ID',

            'image' => 'Виберіть картинку',
            'category_sport' => 'Категорія',
            'kind_sport' => 'Тип',
            'name' => 'Назва',
            'description' => 'Описання',
            'contacts' => 'Контакти',
            'building_number' => 'Номер будівлі',
            'street' => 'Вулиця',
            'city' => 'Місто',
            'district_city' => 'Район міста',
            'area' => 'Район',
            'region' => 'Область',
            'country' => 'Країна',
            'country_short' => 'Абривіатура',
            'coordinates' => 'Coordinates',
            'photos' => 'Фотографії',
            'status' => 'Status',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
        ];
    }
}
