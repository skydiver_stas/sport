<?php

namespace app\module\ownerCabinet\models;

use Yii;

/**
 * This is the model class for table "kinds_sports".
 *
 * @property integer $id
 * @property integer $category_sport_id
 * @property string $name
 * @property string $create_at
 * @property string $update_at
 */
class KindSport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kinds_sports';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_sport_id', 'name'], 'required'],
            [['category_sport_id'], 'integer'],
            [['create_at', 'update_at'], 'safe'],
            [['name'], 'string', 'max' => 60],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_sport_id' => 'Category Sport ID',
            'name' => 'Name',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
        ];
    }

    public static function getKindSport($id) {
        return self::find()
            ->asArray()
            ->select(['id', 'name'])
            ->where(['category_sport_id' => $id])
            ->all();
    }
}
