<?php

namespace app\module\ownerCabinet\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property integer $id
 * @property string $name
 * @property string $short_name
 * @property string $create_at
 * @property string $update_at
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'countries';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'short_name'], 'required'],
            [['create_at', 'update_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['short_name'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'short_name' => 'Short Name',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
        ];
    }
}
