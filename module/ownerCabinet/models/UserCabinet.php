<?php

namespace app\module\ownerCabinet\models;

use Yii;
use app\module\ownerCabinet\models\UserData;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property integer $status
 * @property integer $user_group
 * @property string $aut_key
 * @property string $access_token
 * @property string $create_at
 * @property string $update_at
 */
class UserCabinet extends \yii\db\ActiveRecord
{

    public $old_password;
    public $new_password_1;
    public $new_password_2;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['old_password', 'new_password_1', 'new_password_2'], 'required'],
            [['old_password'], 'checkCorrectOldPassword'],
            [['new_password_1', 'new_password_2'], 'checkCoincidencePassword'],
            ['new_password_1', 'string', 'min' => 4, 'max' => 120 ],
            ['new_password_2', 'string', 'min' => 4, 'max' => 120 ],
            [['status', 'user_group'], 'integer'],
            [['create_at', 'update_at'], 'safe'],
            [['username', 'password', 'email'], 'string', 'max' => 255],
            [['aut_key', 'access_token'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'email' => 'Email',
            'status' => 'Status',
            'user_group' => 'User Group',
            'aut_key' => 'Aut Key',
            'access_token' => 'Access Token',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
            'old_password' => 'Ваший поточний пароль',
            'new_password_1' => 'Новий пароль',
            'new_password_2' => 'Новий пароль',
        ];
    }

    public function checkCorrectOldPassword($attribute, $params)
    {
        $password = self::findOne([
            'username' => Yii::$app->user->identity->username,
        ]);

        if(!Yii::$app->getSecurity()->validatePassword($this->old_password, $password->password))
        {
            $this->addError($attribute, 'Пароль не вірний');
        }
    }

    public function checkCoincidencePassword($attribute, $params)
    {

        $password = self::findOne([
            'username' => Yii::$app->user->identity->username,
        ]);

        if(Yii::$app->getSecurity()->validatePassword($this->new_password_1, $password->password) && Yii::$app->getSecurity()->validatePassword($this->new_password_2, $password->password))
        {
            $this->addError($attribute, 'Новий пароль не може співпадати зі старим');
        }

        if($this->new_password_1 != $this->new_password_2)
        {
            $this->addError($attribute, 'Паролі не співпадають');
        }


    }

    public function getUserData()
    {
        return $this->hasOne(UserData::className(), ['user_id' => 'id']);
    }

    public function getOwnerClub()
    {
        return $this->hasOne(UserClub::className(), ['user_id' => 'id']);
    }

    public static function getUser($id)
    {
        return self::findOne([
            'id' => $id,
            'username' => Yii::$app->user->identity->username,
            'email' => Yii::$app->user->identity->email,
        ]);
    }
}
