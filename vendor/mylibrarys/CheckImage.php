<?php

namespace mylibrarys;

use app\config\Constants;
use phpDocumentor\Reflection\Types\String_;

class CheckImage {

    public static function checkFileAvatar($path, $file, $no_image)
    {
        if($file === null || $file === $no_image)
        {
            return $path.Constants::NOT_IMAGE_AVATAR;
        }

        if(isset($file))
        {
            return $path.$file;
        }
    }

} 