<?php
/**
 * Created by PhpStorm.
 * User: Sky
 * Date: 18.11.16
 * Time: 11:06
 */

namespace mylibrarys;



class ModalWindow {

    public static function WindowMessage($view, $message)
    {


        return ' <div id="openModal" class="modalDialog">
        <div>
            <div class="close">X</div>
            <h3>'.$message.'.</h3>

        </div>
    </div>'
    . $view->registerJsFile('@web/js/password_modal.js', ['depends' => 'yii\web\JqueryAsset']);
    }

}