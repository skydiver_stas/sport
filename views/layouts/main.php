<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\config\Constants;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?php $this->registerCssFile($this->registerCssFile('@web/css/main.css')) ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrapper">

    <header>
        <div class="lincs">
            <div class="link-to-home"><?= Html::a('Додому', ['/']) ?></div>

            <div class="link-header">
                <?= Html::a('Новини', ['/news/index']) ?> &nbsp; | &nbsp;
                <?= Html::a('Івенти', ['/event/index']) ?> &nbsp; | &nbsp;
                <?= Html::a('Спортивні клуби', ['/club/index']) ?> &nbsp; | &nbsp;

                <?php if(Yii::$app->user->isGuest): ?>
                    <?= Html::a('Зареєструватись', ['/security/registration']) ?> &nbsp; | &nbsp;
                    <?= Html::a('Увійти', ['/security/login']) ?>
                <?php endif; ?>

                <?php if(!Yii::$app->user->isGuest && Yii::$app->user->identity->user_group == Constants::USER_GROUP): ?>
                    <?= Html::a('Кабінет', ['/user-cabinet']) ?> &nbsp; | &nbsp;
                    <?= Html::a('Вийти', ['/security/logout']) ?> (<?= Yii::$app->user->identity->username; ?>)
                <?php endif; ?>

                <?php if(!Yii::$app->user->isGuest && Yii::$app->user->identity->user_group == Constants::OWNER_GROUP): ?>
                    <?= Html::a('Кабінет', ['/owner-cabinet']) ?> &nbsp; | &nbsp;
                    <?= Html::a('Вийти', ['/security/logout']) ?> (<?= Yii::$app->user->identity->username; ?>)
                <?php endif; ?>
            </div>
        </div>
    </header>

    <div class="main-container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>


<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
