<?php
use yii\helpers\Url;
use app\config\Constants;
/**
 * Created by PhpStorm.
 * User: Sky
 * Date: 02.01.17
 * Time: 15:14
 */

$this->title = $kind_sport['name'];
$this->params['breadcrumbs'][] = ['label' => 'Спортивні клуби', 'url' => ['club/index']];
$this->params['breadcrumbs'][] = ['label' => $kind_sport->category['name'], 'url' => ['club/kind-sport', 'id' => $kind_sport->category['id']]];
$this->params['breadcrumbs'][] = $kind_sport['name'];
?>

<?php if($clubs_list): ?>
    <?php foreach($clubs_list as $key): ?>
        <?php
            $names_photo = unserialize($key['photos']);
            $name_first_photo = array_shift($names_photo);
            $path_photo = Url::to('@web/'.Constants::PATH_CLUBS_IMAGES.$name_first_photo);
        ?>
        <div class="club-list">
            <a href="<?= Yii::$app->urlManager->createUrl(['club/club', 'id' => $key['id']]) ?>"><div><img class="photo" src="<?= $path_photo ?>"></div></a>
            <a href="<?= Yii::$app->urlManager->createUrl(['club/club', 'id' => $key['id']]) ?>"><div class="name"><h2><?= $key['name'] ?></h2></div></a>
            <div class="description"><?= $key['description'] ?></div>
            <div class="contacts">
                <h3>Контакти</h3>
                <pre><?= $key['contacts'] ?></pre>
            </div>
        </div>
    <?php endforeach; ?>

<?php endif; ?>

