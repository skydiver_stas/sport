<?php
use yii\helpers\Url;
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = $category_sport['name'];
$this->params['breadcrumbs'][] = ['label' => 'Спортивні клуби', 'url' => ['club/index']];
$this->params['breadcrumbs'][] = $category_sport['name'];
?>

<?php if($kinds_sport): ?>
    <?php foreach($kinds_sport as $key): ?>
        <p><?= Html::a($key['name'], ['club/clubs-list', 'id' => $key['id']], ['class' => 'profile-link']) ?></p>
    <?php endforeach; ?>
<?php else: ?>
   <h3> В даній категорії поки нема клубів </h3>
<?php endif; ?>