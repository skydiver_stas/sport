<?php
use yii\helpers\Url;
use yii\helpers\Html;
/* @var $this yii\web\View */
$this->title = 'Cпортивні клуби';
$this->params['breadcrumbs'][] = 'Cпортивні клуби';
?>

<?php foreach($categories as $key): ?>
    <p><?= Html::a($key['name'], ['club/kind-sport', 'id' => $key['id']], ['class' => 'profile-link']) ?></p>
<?php endforeach; ?>