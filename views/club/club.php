<?php
use yii\helpers\Url;
use app\config\Constants;


$names_photo = unserialize($club['photos']);
$name_first_photo = array_shift($names_photo);
$path_photo = Url::to('@web/'.Constants::PATH_CLUBS_IMAGES.$name_first_photo);
?>

<div class="club">

    <div class="club-name"><h2><?= $club['name']?></h2></div>
    <div class="club-photo-div">
        <img class="club-photo" src="<?= $path_photo ?>">
        <?php for($i = 0; $i < count($names_photo); $i++): ?>
            <div class="club-photo-small-div">
                <img class="club-photo-small" src="<?= Url::to('@web/'.Constants::PATH_CLUBS_IMAGES.$names_photo[$i]); ?>">
            </div>
        <?php endfor; ?>
    </div>
    <div class="club-description"><?= $club['description'] ?></div>

</div>